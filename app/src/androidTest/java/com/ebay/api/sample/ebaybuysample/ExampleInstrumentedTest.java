package com.ebay.api.sample.ebaybuysample;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.android.volley.toolbox.HurlStack;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.BasicHttpEntity;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringBufferInputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.swagger.client.ApiException;
import io.swagger.client.ApiInvoker;
import io.swagger.client.api.OrderApi;
import io.swagger.client.model.Address;
import io.swagger.client.model.CheckoutSessionResponse;
import io.swagger.client.model.CreateGuestCheckoutSessionRequest;
import io.swagger.client.model.LineItemInput;
import io.swagger.client.request.PostRequest;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    public static final String TAG = "ExampleInstrumentedTest";
    private static final String API_KEY = "Bearer v^1.1#i^1#f^0#p^3#I^3#r^0#t^H4sIAAAAAAAAAOVXW2wUVRju9oa11AsQJATDOioKdXbOzOzsZWxXti2Ehl5WtoCAgmfOnCnTzs4sc9l2JZHaKCb1BkSC4YUaFNFAuARFVC4xYnioChhJiCY+kBgSVFAi4CWNZ7bbsq0BeuGhifsye878t+//v//M+UFHccmc9QvWXy3zTMjv7gAd+R4PWwpKiovK7yrIn16UB3IEPN0dD3UUdhacr7BgQkuKi7CVNHQLe9sTmm6Jmc1KyjF10YCWaok6TGBLtJEYj9bXiZwPiEnTsA1kaJS3tqaSwpIAFMQJYRlKUgDwZFfvt9lkVFIQ4TAXQhj4ZSnAC5i8tywH1+qWDXW7kuIAG6QBR3OBJsCLvCAKAR8rCMsp7xJsWqqhExEfoCKZcMWMrpkT681DhZaFTZsYoSK10fnxxmhtzbyGpgomx1Ykm4e4DW3HGryqNmTsXQI1B9/cjZWRFuMOQtiyKCbS52GwUTHaH8wows+kOowxhljgBRmHEALh25LK+YaZgPbN43B3VJlWMqIi1m3VTt8qoyQbUgtGdnbVQEzU1njdx5MO1FRFxWYlNa8qumxxfN4iyhuPxUwjpcpYdpGyPA8CgaDAUhEbWySF2FzV6iSgiS2oJ6CuolaYwpqT9dtnPJv1IY6rDV1W3Rxa3gbDrsIEBB6aKi4nVUSoUW80o4rtBpgrJ/SnlCdyTH9RHXu17pYZJ0hevJnlrQvSz5DrnLhdHPFDJexHIQVKLPSzAkd53V4fO08ibqmisRiDJZimSSVasZ3USHPTiKTWSWBTlUW/X/IHIFJonnQ87ZcFRIfDAqY5HMABDvJ+oPj/x1SxbVOVHBsP0GXoiwzmSiqOjCSOGZqK0tRQkcxplCVHu1VJrbbtpMgwbW1tvjbeZ5jNDAcAyzxVXxdHq3ECUgOy6q2FaTVDEUQOaSIv2ukkiaadsJA415upCG/KMWja6SonTdZxrGnk0c/kQRFGhu7eAKrlQh1fIF19ixiASdXnkt2HjARjQNLY7taqTMTe4QgxkpMm/mVs+kwMZUPX0sPXa3YImfu0h6dkkWr43F7v600CZYReMwYGlEego+opwmXDTI/G4YDyCHQgQoaj26Nxl1UdgYbiaIqqaW67jsZhjvpIwtShlrZVZA24HFOXRZPJWnl8ddnC7BFNg4Ak034hCGmJU4I0FtgQkljyF4IxYZZxSkV4lTrOcOuOpo0JV31zFhLp9X3jBlYDEx0TqhqcGm8MRbIscziEaRBEftrPhXlakoISzZLvvBICIVZQ4JgwV2sqORWa0uPtA7jAsGwsjw0auZKOL1BhIPNBWQY05MIcqSaW6DAKucePIPlZBGQU4oYLmbnhde4/l3pm8JAdycv82E7PYdDpOUTmdBAENFsOZhcXLC4smEhZqo195OIqS0a7T4WKz1KbdTJDmtjXitNJqJr5xZ76c68uW5cz3nc/A6YNDPglBWxpzrQPZlx/U8TefV8ZGwQcR+Z0XhACy8GD198WslMLp+wrT0+e9PyGPUcnlD1W3hibGChNvQ3KBoQ8nqK8wk5PXtdzp0ubanYHjBXv1P+wH5RfnZsKHrgo7z2/3+o++eGs399Psft6pGmvXXwi9MtPJ1h56tbAprrFFy5e3vz5JGZh3ZyKF3suU9KFiokP5G86/vXO+7dtvfTCGvuOnafBS9tnHjvRm3fmlDL3HvDKrA7h72+XfTbjG+ncG7EW/Y8/39217b2D97Zv/WD7lk+utR3padrF5PfC3j0Nn555XOt8tHTF2qreR3wzS2hm/ZbpXxwq2sj8eq25ZdaVt5bsPX6lefelNQfQPz+e7H4dHmVWfrdh8sunup79a/rPmzfmHWjpvvP7g/pvWs9XwWOb42flp4+t+ahw6ccnv9whnV3b/fCZpV2z31x5uKtp3ZEdU9r7yvgv4TQaOHgRAAA=";


    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.ebay.api.sample.ebaybuysample", appContext.getPackageName());
    }



    @Test
    public void initiateGuestCheckoutSessionTest() throws Exception {

        try{

            CreateGuestCheckoutSessionRequest guestCheckoutSessionRequest = getCreateGuestCheckoutSessionRequest();

            Log.d(TAG,guestCheckoutSessionRequest.toString());

            OrderApi api = new OrderApi();
            api.setBasePath("https://api.sandbox.ebay.com/buy");
            api.getInvoker().setApiKey(API_KEY);

            String req = ApiInvoker.serialize(guestCheckoutSessionRequest);

            Log.d(TAG,req);

            CheckoutSessionResponse response = api.initiateGuestCheckoutSession(guestCheckoutSessionRequest);

            Log.d(TAG,response.toString());

        }
        catch( ApiException a){
            a.printStackTrace();
            Log.e(TAG,"code " + a.getCode());
            Log.d(TAG," ");

//            System.err.println(a.getResponseBody());

            throw a;
        }
        catch (Exception e){

            e.printStackTrace();

            System.err.println(e.getMessage());

            throw e;
        }
        // TODO: test validations
    }


    public String getRequest(){
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("{");
        stringBuffer.append("    \"contactEmail\": \"fsmith1234@anymail.com\",");
        stringBuffer.append("    \"contactFirstName\": \"Frank\",");
        stringBuffer.append("    \"contactLastName\": \"Smith\",");
        stringBuffer.append("    \"shippingAddress\": {");
        stringBuffer.append("        \"recipient\": \"Frank Smith\",");
//        stringBuffer.append("        \"phoneNumber\": \"617 555 1212\",");
        stringBuffer.append("        \"addressLine1\": \"3737 Any St\",");
        stringBuffer.append("        \"city\": \"San Jose\",");
        stringBuffer.append("        \"stateOrProvince\": \"CA\",");
        stringBuffer.append("        \"postalCode\": \"95134\",");
        stringBuffer.append("        \"country\": \"US\"");
        stringBuffer.append("    },");
        stringBuffer.append("    \"lineItemInputs\": [");
        stringBuffer.append("        {");
        stringBuffer.append("            \"quantity\": 1,");
        stringBuffer.append("            \"itemId\": \"v1|110187573382|0\"");
        stringBuffer.append("        }");
        stringBuffer.append("     ]");
        stringBuffer.append("}");
        Log.d(TAG, stringBuffer.toString());
        return stringBuffer.toString();
    }

    private static BasicHttpEntity entityFromConnection(HttpURLConnection connection) {
        BasicHttpEntity entity = new BasicHttpEntity();
        InputStream inputStream;
        try {
            inputStream = connection.getInputStream();
        } catch (IOException ioe) {
            inputStream = connection.getErrorStream();
        }
        entity.setContent(inputStream);
        entity.setContentLength(connection.getContentLength());
        entity.setContentEncoding(connection.getContentEncoding());
        entity.setContentType(connection.getContentType());
        return entity;
    }

    @Test
    public void testViaHttp() throws Exception {
        Log.d(TAG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");


        byte[] body = getRequest().getBytes();

        URL url = new URL("https://api.sandbox.ebay.com/buy/order/v1/guest_checkout_session/initiate");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        int timeoutMs = 5000;
        connection.setConnectTimeout(timeoutMs);
        connection.setReadTimeout(timeoutMs);
        connection.setUseCaches(false);
        connection.setDoInput(true);

        connection.addRequestProperty("Authorization",API_KEY);
        connection.addRequestProperty("Accept","application/json");
        connection.addRequestProperty("Content-Type","application/json");

        connection.setRequestMethod("POST");
        connection.setDoOutput(true);

        DataOutputStream out = new DataOutputStream(connection.getOutputStream());
        out.write(body);
        out.close();

        int responseCode = connection.getResponseCode();
        String responseMessage = connection.getResponseMessage();
        BasicHttpEntity entity = entityFromConnection(connection);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        entity.writeTo(outputStream);

        String response = new String(outputStream.toByteArray());

        Log.d(TAG,"" + responseCode + " : " + responseMessage);
        Log.d(TAG,response);
        Log.d(TAG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }


    public CreateGuestCheckoutSessionRequest getCreateGuestCheckoutSessionRequest(){

        CreateGuestCheckoutSessionRequest guestCheckoutSessionRequest = new CreateGuestCheckoutSessionRequest();

        guestCheckoutSessionRequest.setContactEmail("fsmith1234@anymail.com");
        guestCheckoutSessionRequest.setContactFirstName("Frank");
        guestCheckoutSessionRequest.setContactLastName("Smith");


        Address address = new Address();
        address.setRecipient("Frank Smith");
        address.setAddressLine1("3737 Any St");
        address.setCity("San Jose");
        address.setStateOrProvince("CA");
        address.setCountry("US");
        address.setPostalCode("95134");
        // TODO Update Swagger to add phone number in address. address.setPhoneNumber(phoneNumber);

        guestCheckoutSessionRequest.setShippingAddress(address);

        List<LineItemInput> lineItemInputs = new ArrayList<>();
        LineItemInput lineItemInput = new LineItemInput();
        lineItemInput.setItemId("v1|110187573382|0");
        lineItemInput.setQuantity(1);
        lineItemInputs.add(lineItemInput);

        guestCheckoutSessionRequest.setLineItemInputs(lineItemInputs);

        return  guestCheckoutSessionRequest;
    }


    @Test
    public void testViaHttpFail() throws Exception {
        Log.d(TAG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        final String API_KEY = "Bearer v^1.1#i^1#f^0#p^3#I^3#r^0#t^H4sIAAAAAAAAAOVXW2wUVRju9oa11AsQJATDOioKdXbOzOzsZWxXti2Ehl5WtoCAgmfOnCnTzs4sc9l2JZHaKCb1BkSC4YUaFNFAuARFVC4xYnioChhJiCY+kBgSVFAi4CWNZ7bbsq0BeuGhifsye878t+//v//M+UFHccmc9QvWXy3zTMjv7gAd+R4PWwpKiovK7yrIn16UB3IEPN0dD3UUdhacr7BgQkuKi7CVNHQLe9sTmm6Jmc1KyjF10YCWaok6TGBLtJEYj9bXiZwPiEnTsA1kaJS3tqaSwpIAFMQJYRlKUgDwZFfvt9lkVFIQ4TAXQhj4ZSnAC5i8tywH1+qWDXW7kuIAG6QBR3OBJsCLvCAKAR8rCMsp7xJsWqqhExEfoCKZcMWMrpkT681DhZaFTZsYoSK10fnxxmhtzbyGpgomx1Ykm4e4DW3HGryqNmTsXQI1B9/cjZWRFuMOQtiyKCbS52GwUTHaH8wows+kOowxhljgBRmHEALh25LK+YaZgPbN43B3VJlWMqIi1m3VTt8qoyQbUgtGdnbVQEzU1njdx5MO1FRFxWYlNa8qumxxfN4iyhuPxUwjpcpYdpGyPA8CgaDAUhEbWySF2FzV6iSgiS2oJ6CuolaYwpqT9dtnPJv1IY6rDV1W3Rxa3gbDrsIEBB6aKi4nVUSoUW80o4rtBpgrJ/SnlCdyTH9RHXu17pYZJ0hevJnlrQvSz5DrnLhdHPFDJexHIQVKLPSzAkd53V4fO08ibqmisRiDJZimSSVasZ3USHPTiKTWSWBTlUW/X/IHIFJonnQ87ZcFRIfDAqY5HMABDvJ+oPj/x1SxbVOVHBsP0GXoiwzmSiqOjCSOGZqK0tRQkcxplCVHu1VJrbbtpMgwbW1tvjbeZ5jNDAcAyzxVXxdHq3ECUgOy6q2FaTVDEUQOaSIv2ukkiaadsJA415upCG/KMWja6SonTdZxrGnk0c/kQRFGhu7eAKrlQh1fIF19ixiASdXnkt2HjARjQNLY7taqTMTe4QgxkpMm/mVs+kwMZUPX0sPXa3YImfu0h6dkkWr43F7v600CZYReMwYGlEego+opwmXDTI/G4YDyCHQgQoaj26Nxl1UdgYbiaIqqaW67jsZhjvpIwtShlrZVZA24HFOXRZPJWnl8ddnC7BFNg4Ak034hCGmJU4I0FtgQkljyF4IxYZZxSkV4lTrOcOuOpo0JV31zFhLp9X3jBlYDEx0TqhqcGm8MRbIscziEaRBEftrPhXlakoISzZLvvBICIVZQ4JgwV2sqORWa0uPtA7jAsGwsjw0auZKOL1BhIPNBWQY05MIcqSaW6DAKucePIPlZBGQU4oYLmbnhde4/l3pm8JAdycv82E7PYdDpOUTmdBAENFsOZhcXLC4smEhZqo195OIqS0a7T4WKz1KbdTJDmtjXitNJqJr5xZ76c68uW5cz3nc/A6YNDPglBWxpzrQPZlx/U8TefV8ZGwQcR+Z0XhACy8GD198WslMLp+wrT0+e9PyGPUcnlD1W3hibGChNvQ3KBoQ8nqK8wk5PXtdzp0ubanYHjBXv1P+wH5RfnZsKHrgo7z2/3+o++eGs399Psft6pGmvXXwi9MtPJ1h56tbAprrFFy5e3vz5JGZh3ZyKF3suU9KFiokP5G86/vXO+7dtvfTCGvuOnafBS9tnHjvRm3fmlDL3HvDKrA7h72+XfTbjG+ncG7EW/Y8/39217b2D97Zv/WD7lk+utR3padrF5PfC3j0Nn555XOt8tHTF2qreR3wzS2hm/ZbpXxwq2sj8eq25ZdaVt5bsPX6lefelNQfQPz+e7H4dHmVWfrdh8sunup79a/rPmzfmHWjpvvP7g/pvWs9XwWOb42flp4+t+ahw6ccnv9whnV3b/fCZpV2z31x5uKtp3ZEdU9r7yvgv4TQaOHgRAAA=";

        CreateGuestCheckoutSessionRequest guestCheckoutSessionRequest = getCreateGuestCheckoutSessionRequest();
        String request = ApiInvoker.serialize(guestCheckoutSessionRequest);
        byte[] body = request.getBytes();
        URL url = new URL("https://api.sandbox.ebay.com/buy/order/v1/guest_checkout_session/initiate");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        int timeoutMs = 5000;
        connection.setConnectTimeout(timeoutMs);
        connection.setReadTimeout(timeoutMs);
        connection.setUseCaches(false);
        connection.setDoInput(true);

        connection.addRequestProperty("User-Agent","Swagger-Codegen/1.0.0/android");
        connection.addRequestProperty("Accept","application/json");
        connection.addRequestProperty("Content-Type","application/json");
        connection.addRequestProperty("Authorization",API_KEY);

        connection.setRequestMethod("POST");
        connection.setDoOutput(true);

        DataOutputStream out = new DataOutputStream(connection.getOutputStream());
        out.write(body);
        out.close();

        int responseCode = connection.getResponseCode();
        String responseMessage = connection.getResponseMessage();
        BasicHttpEntity entity = entityFromConnection(connection);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        entity.writeTo(outputStream);

        String response = new String(outputStream.toByteArray());

        Log.d(TAG,"" + responseCode + " : " + responseMessage);
        Log.d(TAG,response);
        Log.d(TAG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }


    @Test
    public void testHurlStack() throws Exception {
//        HurlStack hurlStack = new HurlStack();
//        PostRequest request = new PostRequest();
//
//        HttpResponse response = hurlStack.performRequest();
    }
}

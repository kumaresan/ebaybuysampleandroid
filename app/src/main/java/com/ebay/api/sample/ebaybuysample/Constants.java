package com.ebay.api.sample.ebaybuysample;

/**
 * All Constants
 * Created by @Kumaresan on 2/24/2017.
 */

public interface Constants {
    public static final String ARG_ITEM_ID = "item_id";
    public static final String ARG_GUEST_CHECKOUT_SESSION = "guest_checkout_session";
    public static final String ARG_ORDER_ID = "order_id";
    public static final String ARG_EMAIL = "email";
}

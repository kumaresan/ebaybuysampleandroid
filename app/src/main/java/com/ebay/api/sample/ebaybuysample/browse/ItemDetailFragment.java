package com.ebay.api.sample.ebaybuysample.browse;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.ebay.api.sample.ebaybuysample.R;
import com.ebay.api.sample.ebaybuysample.apicall.ApiCallback;
import com.ebay.api.sample.ebaybuysample.apicall.GetItem;
import com.ebay.api.sample.ebaybuysample.util.ImageSlider;

import java.util.ArrayList;
import java.util.List;

import io.swagger.client.model.ErrorDetailV3;
import io.swagger.client.model.Image;
import io.swagger.client.model.Item;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment implements ApiCallback {

    private static final String TAG = "ItemDetailFragment";

    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";


    CollapsingToolbarLayout appBarLayout;
    View rootView;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate");

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            // mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

            String itemId = getArguments().getString(ARG_ITEM_ID);


            Activity activity = this.getActivity();
            this.appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(itemId + " Loading ...");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String itemId = getArguments().getString(ARG_ITEM_ID);
        Log.d(TAG, "onCreateView " + itemId);

        this.rootView = inflater.inflate(R.layout.item_detail, container, false);

        GetItem getItem = new GetItem(itemId, this);
        Log.d(TAG, "onCreateView execute getItem");
        getItem.execute();

        return rootView;
    }


    /**
     * Called when the Search Items API call is successful
     *
     * @param response the response received from the api call.
     */
    @Override
    public void handleResponse(Object response) {
        Item item = (Item) response;

        Log.d(TAG, "handleResponse " + item.getItemId());

        appBarLayout.setTitle(item.getTitle());

//        TextView descriptionView = (TextView) this.rootView.findViewById(R.id.item_detail);
//        descriptionView.setText(Html.fromHtml(item.getDescription()));

        List<Image> imageList;

        if (item.getAdditionalImages() != null && item.getAdditionalImages().size() > 0) {
            imageList = item.getAdditionalImages();
        } else {
            imageList = new ArrayList<>();
            imageList.add(item.getImage());
        }

        ViewPager viewPager = (ViewPager) this.rootView.findViewById(R.id.viewPager);
        PagerAdapter adapter = new ImageSlider(this.getActivity(), imageList);
        viewPager.setAdapter(adapter);

        TextView titleTextView = (TextView) this.rootView.findViewById(R.id.title);
        TextView subTitleTextView = (TextView) this.rootView.findViewById(R.id.sub_title);
        TextView priceTextView = (TextView) this.rootView.findViewById(R.id.price);
        TextView conditionTextView = (TextView) this.rootView.findViewById(R.id.condition);
        TextView sellerTextView = (TextView) this.rootView.findViewById(R.id.seller);
        TextView itemLocationTextView = (TextView) this.rootView.findViewById(R.id.item_location);
        TextView shippingOptionTextView = (TextView) this.rootView.findViewById(R.id.shipping_option);

        titleTextView.setText(item.getTitle());
        subTitleTextView.setText(item.getSubtitle());
        priceTextView.setText("$ " + item.getPrice().getValue());
        conditionTextView.setText(item.getCondition());

        if (item.getSeller() != null && item.getSeller().getUsername() != null) {
            sellerTextView.setText(item.getSeller().getUsername());
        }

        itemLocationTextView.setText(item.getItemLocation().getCity() + "," + item.getItemLocation().getCountry());

        if (item.getShippingOptions() != null && item.getShippingOptions().size() > 0) {
            shippingOptionTextView.setText(item.getShippingOptions().get(0).getShippingServiceName());
        }

        WebView descriptionWebView = (WebView) this.rootView.findViewById(R.id.description);
        descriptionWebView.loadDataWithBaseURL("www.ebay.com", item.getDescription(), null, "UTF-8", null);

    }

    /**
     * Called when the Search Items API call returns errors.
     *
     * @param errors errors received from api call or client side exceptions modeled as errors.
     */
    @Override
    public void handleErrors(List<ErrorDetailV3> errors) {
        // TODO do a toast with errors message

        Toast.makeText(getActivity().getApplicationContext(), "Error! @ " + TAG, Toast.LENGTH_SHORT).show();
    }
}

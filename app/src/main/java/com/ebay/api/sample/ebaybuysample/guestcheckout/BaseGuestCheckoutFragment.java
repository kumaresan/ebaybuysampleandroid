package com.ebay.api.sample.ebaybuysample.guestcheckout;

import android.support.v4.app.Fragment;

/**
 * Common feature for all the fragments.
 * Not meant to be used standalone
 * Created by @Kumaresan on 2/24/2017.
 */

public class BaseGuestCheckoutFragment extends Fragment {

    public BaseGuestCheckoutFragment() {
    }

    protected GuestCheckoutSession getGuestCheckoutSession() {
        return ((GuestCheckoutActivity) getActivity()).getGuestCheckoutSession();
    }
}

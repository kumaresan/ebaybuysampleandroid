package com.ebay.api.sample.ebaybuysample.apicall;

import android.util.Log;

/**
 * Get item details from eBay for the given Item Id
 * Created by @Kumaresan on 2/22/2017.
 */

public class GetItem extends BaseApiCall {
    private static final String TAG = "GetItem";
    private String itemId;

    public GetItem(String itemId, ApiCallback apiCallback) {
        super(apiCallback);
        this.itemId = itemId;
    }

    protected void callApiInBackground() throws Exception {
        Log.d(TAG, "callApiInBackground");
        this.response = getBrowseApi().getItem(this.itemId);
    }
}

package com.ebay.api.sample.ebaybuysample.guestcheckout;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ebay.api.sample.ebaybuysample.R;
import com.ebay.api.sample.ebaybuysample.apicall.ApiCallback;
import com.ebay.api.sample.ebaybuysample.apicall.InitiateGuestCheckoutSession;

import java.util.ArrayList;
import java.util.List;

import io.swagger.client.api.OrderApi;
import io.swagger.client.model.Address;
import io.swagger.client.model.CheckoutSessionResponse;
import io.swagger.client.model.CreateGuestCheckoutSessionRequest;
import io.swagger.client.model.ErrorDetailV3;
import io.swagger.client.model.LineItemInput;

/**
 * Shipping Fragment holder during guest checkout
 * Created by @Kumaresan on 2/24/2017.
 */

public class ShippingFragment extends BaseGuestCheckoutFragment implements ApiCallback, View.OnClickListener{
    private static final String TAG = "ShippingFragment";

    View shippingFragmentView;
    EditText emailView;
    EditText firstNameView;
    EditText lastNameView;
    EditText addressView;
    EditText cityView;
    EditText stateView;
    EditText countryView;
    EditText postalCodeView;
    EditText phoneNumberView;

    public ShippingFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ShippingFragment newInstance() {
        Log.d(TAG, "newInstance");
        return new ShippingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.shippingFragmentView = inflater.inflate(R.layout.fragment_guest_checkout_shipping, container, false);
        this.emailView = (EditText)shippingFragmentView.findViewById(R.id.email);
        this.firstNameView = (EditText)shippingFragmentView.findViewById(R.id.first_name);
        this.lastNameView = (EditText)shippingFragmentView.findViewById(R.id.last_name);
        this.addressView = (EditText)shippingFragmentView.findViewById(R.id.address);
        this.cityView = (EditText)shippingFragmentView.findViewById(R.id.city);
        this.stateView = (EditText)shippingFragmentView.findViewById(R.id.state);
        this.countryView = (EditText)shippingFragmentView.findViewById(R.id.country);
        this.postalCodeView = (EditText)shippingFragmentView.findViewById(R.id.postal_code);
        this.phoneNumberView = (EditText)shippingFragmentView.findViewById(R.id.phone_number);

        // TODO these are for test remove these
        this.emailView.setText("fsmith1234@anymail.com");
        this.firstNameView.setText("Frank");
        this.lastNameView.setText("Smith");
        this.phoneNumberView.setText("617 555 1212");

        this.addressView.setText("3737 Any St");
        this.cityView.setText("San Jose");
        this.stateView.setText("CA");
        this.countryView.setText("US");
        this.postalCodeView.setText("95134");

        //TODO set values from the activity.GetCheckoutSession.

        Button b = (Button) shippingFragmentView.findViewById(R.id.shipping_next);
        b.setOnClickListener(this);

        return this.shippingFragmentView;
    }

    @Override
    public void handleResponse(Object response){
        CheckoutSessionResponse checkoutSessionResponse = (CheckoutSessionResponse) response;
        String email = this.emailView.getText().toString();
        String firstName = this.firstNameView.getText().toString();
        String lastName = this.lastNameView.getText().toString();


        ((GuestCheckoutActivity)getActivity()).onShippingContinue(email, firstName, lastName, checkoutSessionResponse);
        Log.d(TAG,checkoutSessionResponse.toString());
    }

    @Override
    public void handleErrors(List<ErrorDetailV3> errors){
        // TODO handle errors
        Toast.makeText(getActivity().getApplicationContext(), "Error! @ " + TAG, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view){

        String itemId = ((GuestCheckoutActivity)getActivity()).getGuestCheckoutSession().itemId;
        Integer quantity = ((GuestCheckoutActivity)getActivity()).getGuestCheckoutSession().quantity;

        String email = this.emailView.getText().toString();
        String firstName = this.firstNameView.getText().toString();
        String lastName = this.lastNameView.getText().toString();
        String addressLine = this.addressView.getText().toString();
        String city = this.cityView.getText().toString();
        String state = this.stateView.getText().toString();
        String country = this.countryView.getText().toString();
        String postalCode = this.postalCodeView.getText().toString();
        String phoneNumber = this.phoneNumberView.getText().toString();

        Log.d(TAG, "onClick " + itemId + " " + quantity);
        Log.d(TAG, "onClick " + email);
        Log.d(TAG, "onClick " + firstName + " " + lastName);
        Log.d(TAG, "onClick " + addressLine);
        Log.d(TAG, "onClick " + city + " " + state + " " + country);
        Log.d(TAG, "onClick " + postalCode + " " + phoneNumber );

        CreateGuestCheckoutSessionRequest guestCheckoutSessionRequest = new CreateGuestCheckoutSessionRequest();

        guestCheckoutSessionRequest.setContactEmail(email);
        guestCheckoutSessionRequest.setContactFirstName(firstName);
        guestCheckoutSessionRequest.setContactLastName(lastName);


        Address address = new Address();
        address.setRecipient(firstName + " " + lastName);
        address.setAddressLine1(addressLine);
        address.setCity(city);
        address.setStateOrProvince(state);
        address.setCountry(country);
        address.setPostalCode(postalCode);
        address.setPhoneNumber(phoneNumber);

        guestCheckoutSessionRequest.setShippingAddress(address);

        List<LineItemInput> lineItemInputs = new ArrayList<>();
        LineItemInput lineItemInput = new LineItemInput();
        lineItemInput.setItemId(itemId);
        lineItemInput.setQuantity(quantity);
        lineItemInputs.add(lineItemInput);

        guestCheckoutSessionRequest.setLineItemInputs(lineItemInputs);

        Log.d(TAG,guestCheckoutSessionRequest.toString());

        InitiateGuestCheckoutSession initiateGuestCheckoutSession = new InitiateGuestCheckoutSession(guestCheckoutSessionRequest,this);
        initiateGuestCheckoutSession.execute();

        // TODO try using the async methods generated by Swagger.
        // initiateGuestCheckoutSession.doit();

    }

//    private CreateGuestCheckoutSessionRequest getTestCreateGuestCheckoutSessionRequest(){
//
//        {
//            "contactEmail": "fsmith1234@anymail.com",
//                "contactFirstName": "Frank",
//                "contactLastName": "Smith",
//                "shippingAddress": {
//            "recipient": "Frank Smith",
//                    "phoneNumber": "617 555 1212",
//                    "addressLine1": "3737 Any St",
//                    "city": "San Jose",
//                    "stateOrProvince": "CA",
//                    "postalCode": "95134",
//                    "country": "US"
//        },
//            "lineItemInputs": [
//            {
//                "quantity": 1,
//                    "itemId": "v1|110187573382|0"
//            }
//            ]
//        }
//
//        CreateGuestCheckoutSessionRequest guestCheckoutSessionRequest = new CreateGuestCheckoutSessionRequest();
//
//        guestCheckoutSessionRequest.setContactEmail("fsmith1234@anymail.com");
//        guestCheckoutSessionRequest.setContactFirstName("Frank");
//        guestCheckoutSessionRequest.setContactLastName("Smith");
//
//
//        Address address = new Address();
//        address.setRecipient("Frank Smith");
//        address.setAddressLine1("3737 Any St");
//        address.setCity("San Jose");
//        address.setStateOrProvince("CA");
//        address.setCountry("US");
//        address.setPostalCode("95134");
//        // TODO Update Swagger to add phone number in address. address.setPhoneNumber(phoneNumber);
//
//        guestCheckoutSessionRequest.setShippingAddress(address);
//
//        List<LineItemInput> lineItemInputs = new ArrayList<>();
//        LineItemInput lineItemInput = new LineItemInput();
//        lineItemInput.setItemId("v1|110187573382|0");
//        lineItemInput.setQuantity(1);
//        lineItemInputs.add(lineItemInput);
//
//        guestCheckoutSessionRequest.setLineItemInputs(lineItemInputs);
//
//        Log.d(TAG,guestCheckoutSessionRequest.toString());
//
//        return guestCheckoutSessionRequest;
//    }

}
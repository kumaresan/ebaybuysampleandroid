package com.ebay.api.sample.ebaybuysample.guestcheckout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.ebay.api.sample.ebaybuysample.Constants;
import com.ebay.api.sample.ebaybuysample.R;
import com.ebay.api.sample.ebaybuysample.browse.ItemDetailActivity;
import com.ebay.api.sample.ebaybuysample.browse.ItemDetailFragment;

import io.swagger.client.model.CheckoutSessionResponse;
import io.swagger.client.model.PurchaseOrderSummary;

/**
 * Handles complete guest checkout flow of a single item.
 * Created by @Kumaresan on 2/22/2017.
 */

public class GuestCheckoutActivity extends AppCompatActivity {

    private static final String TAG = "GuestCheckoutActivity";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter sectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager viewPager;

    private GuestCheckoutSession guestCheckoutSession;

    public GuestCheckoutSession getGuestCheckoutSession() {
        return this.guestCheckoutSession;
    }

    public ViewPager getViewPager() {return this.viewPager;}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_checkout);


        if(savedInstanceState != null){
            this.guestCheckoutSession = (GuestCheckoutSession)savedInstanceState.getSerializable(Constants.ARG_GUEST_CHECKOUT_SESSION);
        } else {
            this.guestCheckoutSession = new GuestCheckoutSession();
            String itemId = getIntent().getStringExtra(Constants.ARG_ITEM_ID);

            if(itemId == null){
                // TODO this is very bad do something.
            }
            this.guestCheckoutSession.itemId = itemId;
            this.getGuestCheckoutSession().quantity = 1;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        //TODO check if the guest checkout session is still via eBay API Call.
        // if not then invalidate the session.
        // make sure that tab 1 is selected.
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(Constants.ARG_GUEST_CHECKOUT_SESSION, guestCheckoutSession);
        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_guest_checkout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private static final String TAG = "SectionsPagerAdapter";

        ItemFragment itemFragment;
        ShippingFragment shippingFragment;
        PaymentFragment paymentFragment;
        ConfirmFragment confirmFragment;


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            itemFragment = ItemFragment.newInstance();
            shippingFragment = ShippingFragment.newInstance();
            paymentFragment = PaymentFragment.newInstance();
            confirmFragment = ConfirmFragment.newInstance();
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "getItem at position " + position);
            switch (position) {
                case 0:
                    return itemFragment;
                case 1:
                    return shippingFragment;
                case 2:
                    return paymentFragment;
                case 3:
                    return confirmFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            Log.d(TAG, "getCount");
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Log.d(TAG, "getPageTitle at position " + position);
            switch (position) {
                case 0:
                    return "Cart";
                case 1:
                    return "Shipping";
                case 2:
                    return "Payment";
                case 3:
                    return "Review";
            }
            return null;
        }
    }

    public void onItemContinueClicked(String itemId, Integer quantity){
        this.guestCheckoutSession.itemId = itemId;
        this.guestCheckoutSession.quantity= quantity;
        this.viewPager.setCurrentItem(this.viewPager.getCurrentItem() + 1);
    }

    public void onShippingContinue(String email, String firstName, String lastName, CheckoutSessionResponse checkoutSessionResponse){
        this.guestCheckoutSession.guestCheckoutSessionId = checkoutSessionResponse.getCheckoutSessionId();

        this.guestCheckoutSession.email = email;
        this.guestCheckoutSession.firstName = firstName;
        this.guestCheckoutSession.lastName = lastName;

        // save the information returned from the checkoutSession
        guestCheckoutSession.updateFromEBayCheckoutSession(checkoutSessionResponse);

        this.viewPager.setCurrentItem(this.viewPager.getCurrentItem() + 1);
    }

    public void onPaymentContinue(CheckoutSessionResponse checkoutSessionResponse){
        this.guestCheckoutSession.guestCheckoutSessionId = checkoutSessionResponse.getCheckoutSessionId();

        this.guestCheckoutSession.updateFromEBayCheckoutSession(checkoutSessionResponse);
        sectionsPagerAdapter.confirmFragment.guestCheckoutSessionUpdated();

        this.viewPager.setCurrentItem(this.viewPager.getCurrentItem() + 1);
    }


    public void onConfirmNext(PurchaseOrderSummary purchaseOrderSummary){
        Log.d(TAG, "onConfirmNext" + purchaseOrderSummary.toString());
        String orderId = purchaseOrderSummary.getPurchaseOrderId();
        String email = guestCheckoutSession.email;

        Context context = getApplicationContext();
        Intent intent = new Intent(context, OrderActivity.class);
        intent.putExtra(Constants.ARG_ORDER_ID, orderId);
        intent.putExtra(Constants.ARG_EMAIL,email);
        context.startActivity(intent);
    }

}

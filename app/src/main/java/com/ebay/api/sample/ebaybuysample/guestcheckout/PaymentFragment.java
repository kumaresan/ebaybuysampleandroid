package com.ebay.api.sample.ebaybuysample.guestcheckout;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ebay.api.sample.ebaybuysample.R;
import com.ebay.api.sample.ebaybuysample.apicall.ApiCallback;
import com.ebay.api.sample.ebaybuysample.apicall.UpdateGuestPaymentInfo;

import java.util.List;

import io.swagger.client.model.BillingAddress;
import io.swagger.client.model.CheckoutSessionResponse;
import io.swagger.client.model.CreditCard;
import io.swagger.client.model.ErrorDetailV3;
import io.swagger.client.model.UpdatePaymentInformation;

/**
 * Payment Fragment holder during guest checkout
 * Created by @Kumaresan on 2/24/2017.
 */
public class PaymentFragment extends BaseGuestCheckoutFragment implements ApiCallback, View.OnClickListener {

    private static final String TAG = "PaymentFragment";

    private View paymentView;
    private Spinner cardTypeSpinner;
    private EditText cardNameEditText;
    private EditText cardNumberEditText;
    private EditText cvvNumberEditText;
    private Spinner expiryMonthSpinner;
    private Spinner expiryYearSpinner;

    public PaymentFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PaymentFragment newInstance( ) {
        Log.d(TAG, "newInstance");
        return new PaymentFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        this.paymentView = inflater.inflate(R.layout.fragment_guest_checkout_payment, container, false);

        this.cardTypeSpinner = (Spinner) paymentView.findViewById(R.id.card_type);
        this.cardNameEditText = (EditText) paymentView.findViewById(R.id.credit_card_name);
        this.cardNumberEditText = (EditText) paymentView.findViewById(R.id.card_number);
        this.cvvNumberEditText = (EditText) paymentView.findViewById(R.id.cvv);
        this.expiryMonthSpinner = (Spinner) paymentView.findViewById(R.id.expiry_month);
        this.expiryYearSpinner = (Spinner) paymentView.findViewById(R.id.expiry_year);

        // TODO these are for test remove
        this.cardNameEditText.setText("Frank Smith");
        this.cardNumberEditText.setText("5100000001598174");
        this.cvvNumberEditText.setText("012");

        Button b = (Button) paymentView.findViewById(R.id.payment_next);
        b.setOnClickListener(this);

        return paymentView;
    }

    @Override
    public void handleResponse(Object response){
        CheckoutSessionResponse checkoutSessionResponse = (CheckoutSessionResponse) response;
        ((GuestCheckoutActivity)getActivity()).onPaymentContinue(checkoutSessionResponse);
        Log.d(TAG,checkoutSessionResponse.toString());
    }

    @Override
    public void handleErrors(List<ErrorDetailV3> errors){
        // TODO handle errors
        Toast.makeText(getActivity().getApplicationContext(), "Error! @ " + TAG, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view){

        GuestCheckoutSession guestCheckoutSession = ((GuestCheckoutActivity)getActivity()).getGuestCheckoutSession();
        String guestCheckoutSessionId = guestCheckoutSession.guestCheckoutSessionId;

        String cardType = this.cardTypeSpinner.getSelectedItem().toString();
        String cardName = this.cardNameEditText.getText().toString();
        String cardNumber = this.cardNumberEditText.getText().toString();
        String cvv = this.cvvNumberEditText.getText().toString();
        String expiryMonth = this.expiryMonthSpinner.getSelectedItem().toString();
        String expiryYear = this.expiryYearSpinner.getSelectedItem().toString();

        Log.d(TAG, "onClick " + cardType);
        Log.d(TAG, "onClick " + cardName);
        Log.d(TAG, "onClick " + cardNumber);
        Log.d(TAG, "onClick " + cvv + " " + expiryMonth + " " + expiryYear);

        UpdatePaymentInformation updatePaymentInformation = new UpdatePaymentInformation();

        CreditCard creditCard = new CreditCard();
        creditCard.setAccountHolderName(cardName);
        creditCard.setBrand(cardType);
        creditCard.setCardNumber(cardNumber);
        creditCard.setCvvNumber(cvv);
        creditCard.setExpireMonth(Integer.parseInt(expiryMonth));
        creditCard.setExpireYear(Integer.parseInt(expiryYear));

        // Same as the shipping address. Saved in previous session.
        BillingAddress address = new BillingAddress();
        address.setFirstName(guestCheckoutSession.firstName);
        address.setLastName(guestCheckoutSession.lastName);
        address.setAddressLine1(guestCheckoutSession.addressLine);
        address.setCity(guestCheckoutSession.city);
        address.setStateOrProvince(guestCheckoutSession.state);
        address.setCountry(guestCheckoutSession.country);
        address.setPostalCode(guestCheckoutSession.postalCode);

        creditCard.setBillingAddress(address);
        updatePaymentInformation.setCreditCard(creditCard);

        Log.d(TAG,updatePaymentInformation.toString());

        UpdateGuestPaymentInfo updateGuestPaymentInfo = new UpdateGuestPaymentInfo(guestCheckoutSessionId,updatePaymentInformation,this);
        updateGuestPaymentInfo.execute();
    }


}
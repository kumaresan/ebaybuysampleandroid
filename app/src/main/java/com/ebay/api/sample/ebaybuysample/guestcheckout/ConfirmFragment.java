package com.ebay.api.sample.ebaybuysample.guestcheckout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ebay.api.sample.ebaybuysample.R;
import com.ebay.api.sample.ebaybuysample.apicall.ApiCallback;
import com.ebay.api.sample.ebaybuysample.apicall.PlaceGuestOrder;
import com.ebay.api.sample.ebaybuysample.apicall.UpdateGuestPaymentInfo;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.swagger.client.model.BillingAddress;
import io.swagger.client.model.CheckoutSessionResponse;
import io.swagger.client.model.CreditCard;
import io.swagger.client.model.ErrorDetailV3;
import io.swagger.client.model.PurchaseOrderSummary;
import io.swagger.client.model.UpdatePaymentInformation;

/**
 * Confirm Fragment holder during guest checkout
 * Created by @Kumaresan on 2/24/2017.
 */

public class ConfirmFragment extends BaseGuestCheckoutFragment implements ApiCallback, View.OnClickListener{
    private static final String TAG = "ConfirmFragment";

        private View confirmView;
//        private TextView guestCheckoutOrderIdTextView;
//        private TextView orderIdTextView;

        public ConfirmFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ConfirmFragment newInstance( ) {
            Log.d(TAG, "newInstance");
            return new ConfirmFragment();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            this.confirmView = inflater.inflate(R.layout.fragment_guest_checkout_confirm, container, false);

//            String guestCheckoutSessionId = ((GuestCheckoutActivity)getActivity()).getGuestCheckoutSession().guestCheckoutSessionId;
//            this.guestCheckoutOrderIdTextView = (TextView) confirmView.findViewById(R.id.guest_checkout_id);
//            this.guestCheckoutOrderIdTextView.setText(guestCheckoutSessionId);
//            this.orderIdTextView = (TextView)confirmView.findViewById(R.id.order_id);

            Button b = (Button) confirmView.findViewById(R.id.confirm_next);
            b.setOnClickListener(this);

            return confirmView;
        }

        public void guestCheckoutSessionUpdated(){
            GuestCheckoutSession guestCheckoutSession = ((GuestCheckoutActivity)getActivity()).getGuestCheckoutSession();

            Picasso.with(getActivity().getApplicationContext()).load(guestCheckoutSession.imageUrl).into(((ImageView) confirmView.findViewById(R.id.item_image)));
            ((TextView)confirmView.findViewById(R.id.title)).setText(guestCheckoutSession.title);
            ((TextView)confirmView.findViewById(R.id.quantity)).setText("" + guestCheckoutSession.quantity);

            ((TextView)confirmView.findViewById(R.id.email)).setText(guestCheckoutSession.email);
            ((TextView)confirmView.findViewById(R.id.first_name)).setText(guestCheckoutSession.firstName);
            ((TextView)confirmView.findViewById(R.id.last_name)).setText(guestCheckoutSession.lastName);
            ((TextView)confirmView.findViewById(R.id.phone_number)).setText(guestCheckoutSession.phoneNumber);

            ((TextView)confirmView.findViewById(R.id.address_line1)).setText(guestCheckoutSession.addressLine);
            ((TextView)confirmView.findViewById(R.id.address_line2)).setText(guestCheckoutSession.city + " "
                    + guestCheckoutSession.state + " " + guestCheckoutSession.postalCode + " " + guestCheckoutSession.country);
            ((TextView)confirmView.findViewById(R.id.shipped_via)).setText(guestCheckoutSession.shippedVia);


            ((TextView)confirmView.findViewById(R.id.credit_card_brand)).setText(guestCheckoutSession.providedCreditCardBrand);
            ((TextView)confirmView.findViewById(R.id.credit_card_number)).setText("****-****-****-" + guestCheckoutSession.providedCreditCardNumber);

            ((TextView)confirmView.findViewById(R.id.subtotal)).setText(guestCheckoutSession.subtotal);
            ((TextView)confirmView.findViewById(R.id.shipping_cost)).setText(guestCheckoutSession.shippingCost);
            ((TextView)confirmView.findViewById(R.id.tax)).setText(guestCheckoutSession.tax);
            ((TextView)confirmView.findViewById(R.id.total)).setText(guestCheckoutSession.total);
        }

        @Override
        public void handleResponse(Object response){
            PurchaseOrderSummary purchaseOrderSummary = (PurchaseOrderSummary) response;
            Log.d(TAG,purchaseOrderSummary.toString());
//            this.orderIdTextView.setText(purchaseOrderSummary.getPurchaseOrderId());
            ((GuestCheckoutActivity)getActivity()).onConfirmNext(purchaseOrderSummary);
        }

        @Override
        public void handleErrors(List<ErrorDetailV3> errors){
            // TODO handle errors
            Toast.makeText(getActivity().getApplicationContext(), "Error! @ " + TAG, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onClick(View view){
            GuestCheckoutSession guestCheckoutSession = ((GuestCheckoutActivity)getActivity()).getGuestCheckoutSession();
            String guestCheckoutSessionId = guestCheckoutSession.guestCheckoutSessionId;

            Log.d(TAG, "onClick " + guestCheckoutSession);

            PlaceGuestOrder placeGuestOrder = new PlaceGuestOrder(guestCheckoutSessionId,this);
            placeGuestOrder.execute();
        }

}
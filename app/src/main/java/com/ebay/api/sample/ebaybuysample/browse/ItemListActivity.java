package com.ebay.api.sample.ebaybuysample.browse;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ebay.api.sample.ebaybuysample.DataHolder;
import com.ebay.api.sample.ebaybuysample.R;
import com.ebay.api.sample.ebaybuysample.apicall.ApiCallback;
import com.ebay.api.sample.ebaybuysample.apicall.SearchItems;
import com.ebay.api.sample.ebaybuysample.browse.ItemDetailActivity;
import com.ebay.api.sample.ebaybuysample.browse.ItemDetailFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.swagger.client.model.ErrorDetailV3;
import io.swagger.client.model.ItemSummary;
import io.swagger.client.model.SearchPagedCollection;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ItemListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, ApiCallback {

    private static final String TAG = "ItemListActivity";

    public static final String DATA_SEARCH_RESULTS = "search_results";
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private SearchView searchView;
    private MenuItem searchMenuItem;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        this.recyclerView = (RecyclerView) findViewById(R.id.item_list);
        assert this.recyclerView != null;

        List<ItemSummary> itemSummaries = (List<ItemSummary>) DataHolder.getInstance().get(DATA_SEARCH_RESULTS);

        if(itemSummaries != null){
            this.recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(itemSummaries));
        }

        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

    }


    /**
     * Menu icons are inflated just as they were with actionbar
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.appmenu, menu);


        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        this.searchMenuItem = menu.findItem(R.id.action_search);
        this.searchView = (SearchView) this.searchMenuItem.getActionView();

        this.searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        this.searchView.setSubmitButtonEnabled(true);
        this.searchView.setOnQueryTextListener(this);

        return true;
    }

    /**
     * Called from SearchView when there is a chance in text. NoOp for now.
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d(TAG, "onQueryTextChange " + newText);
        return true;
    }

    /**
     * Called from SearchView when the search query is submitted
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d(TAG, "onQueryTextSubmit " + query);

        this.searchMenuItem.collapseActionView();
        this.searchView.setQuery("", false);
        this.searchView.clearFocus();
        this.searchView.setIconified(true);

        SearchItems searchItems = new SearchItems(query, this);
        // clean cache
        DataHolder.getInstance().remove(DATA_SEARCH_RESULTS);

        Log.d(TAG, "onQueryTextSubmit calling search");
        searchItems.execute();

        return true;
    }


    /**
     * Called when the Search Items API call is successful
     *
     * @param response the response received from the api call.
     */
    @Override
    public void handleResponse(Object response) {
        SearchPagedCollection searchPagedCollection = (SearchPagedCollection) response;
        Log.d(TAG, "handleResponse " + searchPagedCollection.getHref());

        DataHolder.getInstance().put(DATA_SEARCH_RESULTS,searchPagedCollection.getItemSummaries());
        List<ItemSummary> itemSummaries = (List<ItemSummary>)DataHolder.getInstance().get(DATA_SEARCH_RESULTS);

        this.recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(itemSummaries));
    }

    /**
     * Called when the Search Items API call returns errors.
     *
     * @param errors errors received from api call or client side exceptions modeled as errors.
     */
    @Override
    public void handleErrors(List<ErrorDetailV3> errors) {
        // TODO do a toast with errors message

        Toast.makeText(getApplicationContext(), "Error! @ " + TAG, Toast.LENGTH_SHORT).show();
    }


    class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<ItemSummary> itemSummaries;

        SimpleItemRecyclerViewAdapter(List<ItemSummary> itemSummaries) {
            this.itemSummaries = itemSummaries;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.setItemSummary(itemSummaries.get(position));
        }

        @Override
        public int getItemCount() {
            if(itemSummaries == null) return 0;
            return itemSummaries.size();
        }


        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            final View mView;
            final ImageView mItemImageView;
            final TextView mTitleTextView;
            final TextView mPriceTextView;
            final TextView mConditionTextView;
            final TextView mSellerTextView;

            private ItemSummary mItem;

            ViewHolder(View view) {
                super(view);
                mView = view;

                mItemImageView = (ImageView) view.findViewById(R.id.item_image);
                mTitleTextView = (TextView) view.findViewById(R.id.title);
                mPriceTextView = (TextView) view.findViewById(R.id.price);
                mConditionTextView = (TextView) view.findViewById(R.id.condition);
                mSellerTextView = (TextView) view.findViewById(R.id.seller);
            }

            public void setItemSummary(ItemSummary mItem){
                this.mItem = mItem;

                Picasso.with(getApplicationContext()).load(this.mItem.getImage().getImageUrl()).into(this.mItemImageView);
                this.mTitleTextView.setText(this.mItem.getTitle());
                this.mPriceTextView.setText("$ " + this.mItem.getPrice().getValue());
                this.mConditionTextView.setText( " | " + this.mItem.getCondition());

                if(this.mItem.getSeller() != null && this.mItem.getSeller().getUsername() != null) {
                    this.mSellerTextView.setText(" | " + this.mItem.getSeller().getUsername());
                }

                this.mView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString(ItemDetailFragment.ARG_ITEM_ID, this.mItem.getItemId());
                    ItemDetailFragment fragment = new ItemDetailFragment();
                    fragment.setArguments(arguments);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_ID, this.mItem.getItemId());

                    context.startActivity(intent);
                }
            }



            @Override
            public String toString() {
                return super.toString() + " '" + mItem + "'";
            }
        }
    }
}

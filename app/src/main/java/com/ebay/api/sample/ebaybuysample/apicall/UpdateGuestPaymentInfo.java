package com.ebay.api.sample.ebaybuysample.apicall;

import android.util.Log;

import io.swagger.client.model.CheckoutSessionResponse;
import io.swagger.client.model.CreateGuestCheckoutSessionRequest;
import io.swagger.client.model.UpdatePaymentInformation;

/**
 * Created by @Kumaresan on 2/25/2017.
 */

public class UpdateGuestPaymentInfo extends BaseApiCall {

    private static final String TAG = "UpdateGuestPaymentInfo";
    private String guestCheckoutSessionId;
    private UpdatePaymentInformation updatePaymentInformation;

    public UpdateGuestPaymentInfo(String guestCheckoutSessionId, UpdatePaymentInformation updatePaymentInformation, ApiCallback apiCallback) {
        super(apiCallback);
        this.guestCheckoutSessionId = guestCheckoutSessionId;
        this.updatePaymentInformation = updatePaymentInformation;
    }

    protected void callApiInBackground() throws Exception {
        Log.d(TAG, "callApiInBackground");
        //this.response = getOrderApi().updateGuestPaymentInfo(this.guestCheckoutSessionId,this.updatePaymentInformation);
        callApiInBackgroundHACK();
    }

    // THIS IS A HACK, replace with original later.
    protected void callApiInBackgroundHACK() throws Exception {
        Log.d(TAG, "callApiInBackground with HACK");

        //https://api.sandbox.ebay.com/buy/order/v1/guest_checkout_session/{guest_checkoutsession_id}/update_payment_info

        String url = "/order/v1/guest_checkout_session/" + this.guestCheckoutSessionId + "/update_payment_info";

        apiCallWithHACK(url,this.updatePaymentInformation, CheckoutSessionResponse.class);
    }


}

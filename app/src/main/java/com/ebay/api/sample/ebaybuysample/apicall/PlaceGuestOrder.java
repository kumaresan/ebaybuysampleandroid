package com.ebay.api.sample.ebaybuysample.apicall;

import android.util.Log;

import io.swagger.client.model.CheckoutSessionResponse;
import io.swagger.client.model.PurchaseOrderSummary;
import io.swagger.client.model.UpdatePaymentInformation;

/**
 * Created by @Kumaresan on 2/26/2017.
 */

public class PlaceGuestOrder extends BaseApiCall {

    private static final String TAG = "UpdateGuestPaymentInfo";
    private String guestCheckoutSessionId;

    public PlaceGuestOrder(String guestCheckoutSessionId, ApiCallback apiCallback) {
        super(apiCallback);
        this.guestCheckoutSessionId = guestCheckoutSessionId;
    }

    protected void callApiInBackground() throws Exception {
        Log.d(TAG, "callApiInBackground");
//        this.response = getOrderApi().placeGuestOrder(this.guestCheckoutSessionId);
        callApiInBackgroundHACK();
    }

    // THIS IS A HACK, replace with original later.
    protected void callApiInBackgroundHACK() throws Exception {
        Log.d(TAG, "callApiInBackground with HACK");

        //https://api.sandbox.ebay.com/buy/order/v1/guest_checkout_session/{guest_checkoutsession_id}/place_order

        String url = "/order/v1/guest_checkout_session/" + this.guestCheckoutSessionId + "/place_order";

        apiCallWithHACK(url,null, PurchaseOrderSummary.class);
    }
}

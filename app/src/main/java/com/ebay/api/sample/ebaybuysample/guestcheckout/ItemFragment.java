package com.ebay.api.sample.ebaybuysample.guestcheckout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ebay.api.sample.ebaybuysample.R;
import com.ebay.api.sample.ebaybuysample.apicall.ApiCallback;
import com.ebay.api.sample.ebaybuysample.apicall.GetItem;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.swagger.client.model.ErrorDetailV3;
import io.swagger.client.model.Item;

/**
 * Item Fragment holder during guest checkout
 * Created by @Kumaresan on 2/24/2017.
 */

public class ItemFragment extends Fragment implements ApiCallback, View.OnClickListener {
    private static final String TAG = "ItemFragment";

    View itemFragmentView;
    EditText quantityView;


    public ItemFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ItemFragment newInstance() {
        Log.d(TAG, "newInstance");
        return new ItemFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        GuestCheckoutSession session = ((GuestCheckoutActivity)getActivity()).getGuestCheckoutSession();

        Log.d(TAG,"onCreateView " + session.itemId);
        Log.d(TAG,"onCreateView " + session.quantity);

        this.itemFragmentView = inflater.inflate(R.layout.fragment_guest_checkout_item, container, false);

        this.quantityView = (EditText)itemFragmentView.findViewById(R.id.quantity);
        quantityView.setText(session.quantity.toString());

        Button b = (Button) itemFragmentView.findViewById(R.id.item_next);
        b.setOnClickListener(this);

        return itemFragmentView;
    }

    @Override
    public void onStart(){
        super.onStart();
        GuestCheckoutSession session = ((GuestCheckoutActivity)getActivity()).getGuestCheckoutSession();
        GetItem getItem = new GetItem(session.itemId,this);
        getItem.execute();
    }

    @Override
    public void handleResponse(Object response){
        Item item = (Item)response;

        ImageView itemImageView = (ImageView) this.itemFragmentView.findViewById(R.id.item_image);
        TextView titleTextView = (TextView) this.itemFragmentView.findViewById(R.id.title);
        TextView priceTextView = (TextView) this.itemFragmentView.findViewById(R.id.price);
        TextView conditionTextView = (TextView) this.itemFragmentView.findViewById(R.id.condition);

        Picasso.with(getActivity().getApplicationContext()).load(item.getImage().getImageUrl()).into(itemImageView);
        titleTextView.setText(item.getTitle());
        priceTextView.setText("$ " + item.getPrice().getValue());
        conditionTextView.setText(item.getCondition());

    }

    @Override
    public void handleErrors(List<ErrorDetailV3> errors){
        // TODO handle errors
        Toast.makeText(getActivity().getApplicationContext(), "Error! @ " + TAG, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view)
    {
        GuestCheckoutSession session = ((GuestCheckoutActivity)getActivity()).getGuestCheckoutSession();
        String itemId = session.itemId;
        Integer quantity = Integer.parseInt(this.quantityView.getText().toString());
        Log.d(TAG, "onClick " + itemId + " " + quantity);

        ((GuestCheckoutActivity)getActivity()).onItemContinueClicked(itemId,quantity);
    }

}
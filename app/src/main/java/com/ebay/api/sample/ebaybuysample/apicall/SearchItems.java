package com.ebay.api.sample.ebaybuysample.apicall;

import android.util.Log;

/**
 * Search Items in eBay
 * Created by @Kumaresan on 2/22/2017.
 */

public class SearchItems extends BaseApiCall {

    private static final String TAG = "SearchItems";
    private String query;

    public SearchItems(String query, ApiCallback apiCallback) {
        super(apiCallback);
        this.query = query;
    }

    protected void callApiInBackground() throws Exception {
        Log.d(TAG, "callApiInBackground");
        this.response = getBrowseApi().searchForItems(this.query, null, "10", "0", null, null);
    }
}

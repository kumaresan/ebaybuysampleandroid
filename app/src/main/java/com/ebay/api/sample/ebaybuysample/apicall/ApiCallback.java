package com.ebay.api.sample.ebaybuysample.apicall;

import java.util.List;

import io.swagger.client.model.ErrorDetailV3;

/**
 * Classes implementing this interface can receive API responses
 * Created by @Kumaresan on 2/22/2017.
 */

public interface ApiCallback {
    /**
     * Called when the Search Items API call is successful
     *
     * @param response the response received from the api call.
     */
    void handleResponse(Object response);

    /**
     * Called when the Search Items API call returns errors.
     *
     * @param errors errors received from api call or client side exceptions modeled as errors.
     */
    void handleErrors(List<ErrorDetailV3> errors);
}
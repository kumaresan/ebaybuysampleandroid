package com.ebay.api.sample.ebaybuysample.apicall;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;

import io.swagger.client.model.CheckoutSessionResponse;
import io.swagger.client.model.CreateGuestCheckoutSessionRequest;
import io.swagger.client.model.ErrorDetailV3;

/**
 * Created by @Kumaresan on 2/24/2017.
 */

public class InitiateGuestCheckoutSession extends BaseApiCall  implements Response.Listener<CheckoutSessionResponse>, Response.ErrorListener {
        private static final String TAG = "GetGuestCheckoutSession";
        private CreateGuestCheckoutSessionRequest  createGuestCheckoutSessionRequest;

        public InitiateGuestCheckoutSession(CreateGuestCheckoutSessionRequest createGuestCheckoutSessionRequest, ApiCallback apiCallback) {
            super(apiCallback);
            this.createGuestCheckoutSessionRequest = createGuestCheckoutSessionRequest;
        }

        protected void callApiInBackgroundOriginal() throws Exception {
            Log.d(TAG, "callApiInBackground");
            this.response = getOrderApi().initiateGuestCheckoutSession(this.createGuestCheckoutSessionRequest);
        }

        // THIS IS A HACK, replace with original later.
        protected void callApiInBackground() throws Exception {
            Log.d(TAG, "callApiInBackground with HACK");
            apiCallWithHACK("/order/v1/guest_checkout_session/initiate",this.createGuestCheckoutSessionRequest, CheckoutSessionResponse.class);

        }

        public void doit() {
            try {
                getOrderApi().initiateGuestCheckoutSession(createGuestCheckoutSessionRequest, this, this);
            } catch (Exception e) {
                VolleyError error = new VolleyError(e);
                onErrorResponse(error);
            }
        }

        @Override
        public void onResponse(CheckoutSessionResponse response) {
            apiCallback.handleResponse(response);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            errors = new ArrayList<>();
            ErrorDetailV3 apiError = new ErrorDetailV3();
            apiError.setMessage(error.getMessage());
            errors.add(apiError);
            apiCallback.handleErrors(errors);
        }
}

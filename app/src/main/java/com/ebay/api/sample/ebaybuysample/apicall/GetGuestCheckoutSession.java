package com.ebay.api.sample.ebaybuysample.apicall;

import android.util.Log;

/**
 * Get the Checkout Session id from eBay
 * Created by @Kumaresan on 2/22/2017.
 */

public class GetGuestCheckoutSession extends BaseApiCall {
    private static final String TAG = "GetGuestCheckoutSession";
    private String guestCheckoutSessionId;

    public GetGuestCheckoutSession(String guestCheckoutSessionId, ApiCallback apiCallback) {
        super(apiCallback);
        this.guestCheckoutSessionId = guestCheckoutSessionId;
    }

    protected void callApiInBackground() throws Exception {
        Log.d(TAG, "callApiInBackground");
        this.response = getOrderApi().getGuestCheckoutSession(this.guestCheckoutSessionId);
    }

}

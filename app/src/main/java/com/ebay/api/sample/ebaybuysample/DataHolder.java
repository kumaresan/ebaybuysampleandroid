package com.ebay.api.sample.ebaybuysample;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple cache to share data between activities
 * Created by @Kumaresan on 2/22/2017.
 */

public class DataHolder {
    private static DataHolder _instance;

    Map<String, WeakReference<Object>> data = new HashMap<String, WeakReference<Object>>();

    private DataHolder() {
    }

    public static DataHolder getInstance() {
        if (_instance == null) {
            _instance = new DataHolder();
        }
        return _instance;
    }

    public void put(String id, Object object) {
        data.put(id, new WeakReference<Object>(object));
    }

    public Object get(String id) {
        WeakReference<Object> objectWeakReference = data.get(id);

        if(objectWeakReference == null)
            return null;

        return objectWeakReference.get();
    }

    public void remove(String id){
        data.remove(id);
    }
}

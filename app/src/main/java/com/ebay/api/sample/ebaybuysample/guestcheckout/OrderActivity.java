package com.ebay.api.sample.ebaybuysample.guestcheckout;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ebay.api.sample.ebaybuysample.Constants;
import com.ebay.api.sample.ebaybuysample.R;
import com.ebay.api.sample.ebaybuysample.apicall.ApiCallback;
import com.ebay.api.sample.ebaybuysample.apicall.GetGuestPurchaseOrder;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.swagger.client.model.ErrorDetailV3;
import io.swagger.client.model.GuestPurchaseOrder;

/**
 *
 * Created by @Kumaresan on 3/1/2017.
 */

public class OrderActivity extends AppCompatActivity implements ApiCallback {
    private static final String TAG = "GuestCheckoutActivity";

    String orderId;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_purchase_order);

        if(savedInstanceState != null){
            this.orderId = savedInstanceState.getString(Constants.ARG_ORDER_ID);
            this.email = savedInstanceState.getString(Constants.ARG_EMAIL);
        } else {
            this.orderId = getIntent().getStringExtra(Constants.ARG_ORDER_ID);
            this.email = getIntent().getStringExtra(Constants.ARG_EMAIL);

            if(this.orderId == null){
                // TODO this is very bad do something.
            }
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        GetGuestPurchaseOrder getGuestPurchaseOrder = new GetGuestPurchaseOrder(this.orderId,this);
        getGuestPurchaseOrder.execute();
    }

    @Override
    public void handleResponse(Object response){
        GuestPurchaseOrder guestPurchaseOrder = (GuestPurchaseOrder)response;


        ((TextView) findViewById(R.id.email)).setText(this.email);

        ImageView itemImageView = (ImageView)findViewById(R.id.item_image);
        Picasso.with(getApplicationContext()).load(guestPurchaseOrder.getLineItems().get(0).getImage().getImageUrl()).into(itemImageView);
        ((TextView) findViewById(R.id.title)).setText(guestPurchaseOrder.getLineItems().get(0).getTitle());

        ((TextView) findViewById(R.id.price)).setText("$ " + guestPurchaseOrder.getPricingSummary().getTotal().getValue());
        ((TextView) findViewById(R.id.quantity)).setText("" + guestPurchaseOrder.getLineItems().get(0).getQuantity());
        ((TextView) findViewById(R.id.seller)).setText(guestPurchaseOrder.getLineItems().get(0).getSeller().getUsername());
        ((TextView) findViewById(R.id.shipped_via)).setText(guestPurchaseOrder.getLineItems().get(0).getShippingDetail().getShippingServiceName());

        String deliveryEstimate = guestPurchaseOrder.getLineItems().get(0).getShippingDetail().getMinEstimatedDeliveryDate()
                + " to "
                + guestPurchaseOrder.getLineItems().get(0).getShippingDetail().getMaxEstimatedDeliveryDate();

        ((TextView) findViewById(R.id.estimated_delivery)).setText(deliveryEstimate);
    }

    @Override
    public void handleErrors(List<ErrorDetailV3> errors){
        // TODO handle errors
        Toast.makeText(getApplicationContext(), "Error! @ " + TAG, Toast.LENGTH_SHORT).show();
    }


}


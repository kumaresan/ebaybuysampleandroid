package com.ebay.api.sample.ebaybuysample.apicall;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.entity.BasicHttpEntity;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.swagger.client.ApiException;
import io.swagger.client.ApiInvoker;
import io.swagger.client.api.BrowseApi;
import io.swagger.client.api.OrderApi;
import io.swagger.client.model.CreateGuestCheckoutSessionRequest;
import io.swagger.client.model.ErrorDetailV3;
import io.swagger.client.model.ErrorResponse;

/**
 * Base class for all eBay API Calls
 * Created by @Kumaresan on 2/22/2017.
 */

abstract class BaseApiCall extends AsyncTask<Void, Integer, Long> {
    private static final String TAG = "BaseApiCall";

    private  final String API_KEY = "Bearer v^1.1#i^1#r^0#p^3#I^3#f^0#t^H4sIAAAAAAAAAOVXW2wUVRjudtuSBvCCSEEgLlNDkDKzZ2Znd2cndJOlBVmh7dpdbk2gOTNzth07O7OZM9N2ieLaGBAxNA1RGxJDicEgAlFiNA36ICo+EOUBSdSoCCQC8QkvXNRQz2y3S1sD9MJDE/dl9pz5b9//f/+Z84NsWfnS7au3X5/pmlbclwXZYpeLnQ7Ky0qrHnAXP1ZaBIYJuPqyT2RLutyXl2OY0tJiI8JpQ8fI05nSdCzmNqsp29RFA2IVizpMISxashiP1K0VOQaIadOwDNnQKE+0tppi/VIgoKAkJ0iSn/XxZFcfspkwqqlAEASJgAI4QQCcTyDvMbZRVMcW1K1qigNskAY+GrAJjhP9rAgEhgg2UZ71yMSqoRMRBlDhXLhiTtccFuvdQ4UYI9MiRqhwNLIq3hCJ1q6sTyz3DrMVzuchbkHLxiNXNYaCPOuhZqO7u8E5aTFuyzLCmPKGBz2MNCpGhoKZQPi5VCMQVHhOkXkUgFAIhe5LKlcZZgpad4/D2VEVOpkTFZFuqVbmXhkl2ZCeRbKVX9UTE9Faj/N4xoaamlSRWU2tXBHZtC6+spHyxGMx02hXFaTkSOXzgUAg6GepsIUwSSEym9vsFDQRhnoK6qrcBtuRZuf9DhrPZ32U4xpDV1Qnh9hTb1grEAGBRqeKH5YqItSgN5iRpOUEWJDjEgDkUxoUhCanxoNFta1W3SkzSpG8eHLLexdkiCG3OXG/OMJxQAgpSghAEJCTQT/lcXp98jwJO6WKxGJeJMEMTSrRhqy0BmVEyyS1dgqZqiLyvMQHoJykfZwg07zil+lQyI9oDgVQgIM+HiT5/zFVLMtUJdtCBbqMfpHDXE3FZSONYoamyhlqtEjuNMqToxNXU62WlRa93o6ODqbDxxhmi5cDgPVurFsbl1tRClIFWfXewrSao4iMiBZWRSuTJtF0EhYS53oLFfaZSgyaVmaFnSHrONI08hhi8ogIw6N37wAVO1CnFkhHHxMDMK0yDtkZ2Uh5DUga29lqzkXsGYuQV7IzxL+CTMZEUDF0LTN2vRabkHlQe2xKmFSDcXp9sDcJlHF6zRkoKI9DR9XbCZcNMzMRhwXlcehAWTZs3ZqIu7zqODSStpZUNc1p14k4HKY+njB1qGUsVcYFl5Pqskg6HVWmVpetyR/RNAhICs37g5CWuGSQRn5WkCWW/IVgUpgV1K7KqFmdYrh1W9MmhauuJQ+J9Pr7UwZWvTcyKVS1qH2qMVRWFIVDAqJBUOZpngv5aEkKSjRLvvNJAQisPwknhblGU8mpkMhMtQ/gagNbSJkcNHIlnVqgQkDxBckUSkMuxJFqIokOyYJz/PglnpWBIgvcWCF773id+8+l3jtyyA4X5X5sl+sT0OXqJ3M6CAKarQJPlrnXlbhnUFi1EEMuropkdDIqTDJYbdHJDGkipg1l0lA1i8tcdRdf3fTCsPG+bzOYWxjwy93s9GHTPlhw+00p+2DFTDYIfIDlOD8LhCZQefttCTunZHa2au97D12t+qjodHlzb+jNS1fcHx8FMwtCLldpUUmXq+jM+T/2hc8f+P7K5rPzd/+FTy1546q9tfLIvu4vF76cuuje2tC94/m+E8Wfzn0ts/StgeKlmw/+PTBt1qy6rs97G0sStw41buruPPLjsf7fDzdcefFA66K+too1/Tu3bVm87+RudqG5J3Dq6aIbA3uP36DevVT5aGX/vJ55/+y4Zf90Rlq36+u3Hzl9M3Lz8OKBjbFd15vqircKvZ91buh5+Is9NfM3iPBk4rDV/VJZomfJhyxj7r/wiv/b0tm1c547s+31o51Pgf29/Iy+a9l007VQ2U7POe2YO3rh1gf0oiV9y0reWXjil2Xlv2bD2bM/HPw5HVuwx9P7zeOXK776zmhKdp08V3FoS/b4b6v/ZAbL+C8TuVEieBEAAA==";
    private static final String SANDBOX_BASE_PATH = "https://api.sandbox.ebay.com/buy";

    private BrowseApi browseApi;
    private OrderApi orderApi;
    protected ApiCallback apiCallback;

    protected List<ErrorDetailV3> errors;
    protected Object response;

    BaseApiCall(ApiCallback apiCallback) {
        this.apiCallback = apiCallback;
        this.browseApi = new BrowseApi();
        this.orderApi = new OrderApi();
        browseApi.getInvoker().setApiKey(API_KEY);
        orderApi.getInvoker().setApiKey(API_KEY);

        //TODO  Sandbox vs Production
        browseApi.setBasePath(SANDBOX_BASE_PATH);
        orderApi.setBasePath(SANDBOX_BASE_PATH);
    }

    BrowseApi getBrowseApi() {
        return this.browseApi;
    }

    OrderApi getOrderApi() {
        return this.orderApi;
    }

    @Override
    protected Long doInBackground(Void... params) {
        Log.d(TAG, "doInBackground ");
        try {
            Log.d(TAG, "doInBackground Call API");

            callApiInBackground();

            Log.d(TAG, "doInBackground Called API");
        } catch (ApiException e) {
            e.printStackTrace();
            Log.e(TAG, "doInBackground Error! " + e.getMessage());

            this.errors = new ArrayList<>();
            ErrorDetailV3 error = new ErrorDetailV3();
            error.setMessage(e.getMessage());
            this.errors.add(error);

            return 1L;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "doInBackground Error! " + e.getMessage());

            this.errors = new ArrayList<>();
            ErrorDetailV3 error = new ErrorDetailV3();
            error.setMessage(e.getMessage());
            this.errors.add(error);

            return 1L;
        }
        Log.d(TAG, "doInBackground Returning 0");
        return 0L;
    }

    @Override
    protected void onPostExecute(Long result) {
        Log.d(TAG, "onPostExecute result " + result);
        if (result == 0L) {
            apiCallback.handleResponse(this.response);
        } else {
            apiCallback.handleErrors(this.errors);
        }
    }


    // THIS IS a HACK
    // all post call via the swagger client are failing so calling the api directly
    // will remove this once the base issue is fixed.
    protected void apiCallWithHACK(String apiUrl, Object requestObject, Class cls) throws Exception {

        // TODO SANDBOX vs Production
        String path = SANDBOX_BASE_PATH + apiUrl;
        Log.d(TAG, path);

        URL url = new URL(path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        int timeoutMs = 30000;
        connection.setConnectTimeout(timeoutMs);
        connection.setReadTimeout(timeoutMs);
        connection.setUseCaches(false);
        connection.setDoInput(true);

        connection.addRequestProperty("User-Agent", "Swagger-Codegen/1.0.0/android");
        connection.addRequestProperty("Accept", "application/json");
        connection.addRequestProperty("Content-Type", "application/json");
        connection.addRequestProperty("Authorization", API_KEY);

        connection.setRequestMethod("POST");

        if(requestObject != null) {
            String request = ApiInvoker.serialize(requestObject);
            Log.d(TAG, request);
            byte[] body = request.getBytes();
            connection.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(connection.getOutputStream());
            out.write(body);
            out.close();
        }

        int responseCode = connection.getResponseCode();
        String responseMessage = connection.getResponseMessage();

        Log.d(TAG, responseMessage + " " + responseCode);

        BasicHttpEntity entity = new BasicHttpEntity();
        InputStream inputStream;
        try {
            inputStream = connection.getInputStream();
        } catch (IOException ioe) {
            inputStream = connection.getErrorStream();
        }
        entity.setContent(inputStream);
        entity.setContentLength(connection.getContentLength());
        entity.setContentEncoding(connection.getContentEncoding());
        entity.setContentType(connection.getContentType());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        entity.writeTo(outputStream);

        String responseString = new String(outputStream.toByteArray());
        Log.d(TAG, responseString);

        if (responseCode == 200) {
            this.response = ApiInvoker.deserialize(responseString, "", cls);
            Log.d(TAG, this.response.toString());
        } else {
            ErrorResponse errorResponse = (ErrorResponse) ApiInvoker.deserialize(responseString, "", ErrorResponse.class);
            this.errors = errorResponse.getErrors();
            Log.d(TAG, errorResponse.toString());
            throw new Exception(this.errors.get(0).getMessage());
        }
    }

    /**
     * Called by the background thread.
     *
     * @throws Exception
     */
    protected abstract void callApiInBackground() throws Exception;
}

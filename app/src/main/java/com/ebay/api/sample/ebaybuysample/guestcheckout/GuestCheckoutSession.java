package com.ebay.api.sample.ebaybuysample.guestcheckout;

import android.util.Log;

import java.io.Serializable;

import io.swagger.client.model.CheckoutSessionResponse;

/**
 * Session object to maintain state across checkout fragments
 * Created by @Kumaresan on 2/24/2017.
 */

public class GuestCheckoutSession implements Serializable {
    private static final String TAG = "GuestCheckoutSession";

    String itemId;
    String title;
    String imageUrl;
    Integer quantity;

    String email;
    String firstName;
    String lastName;

    String guestCheckoutSessionId;

    String addressLine;
    String city;
    String state;
    String country;
    String postalCode;
    String phoneNumber;

    String subtotal;
    String shippingCost;
    String tax;
    String total;

    String shippedVia;

    String providedCreditCardBrand;
    String providedCreditCardNumber;


    public void updateFromEBayCheckoutSession(CheckoutSessionResponse checkoutSessionResponse){
        Log.d(TAG,"updateFromEBayCheckoutSession");
        Log.d(TAG,checkoutSessionResponse.toString());

        this.guestCheckoutSessionId = checkoutSessionResponse.getCheckoutSessionId();

        if(checkoutSessionResponse.getLineItems() != null && checkoutSessionResponse.getLineItems().size() > 0){
            this.itemId = checkoutSessionResponse.getLineItems().get(0).getItemId();
            this.title = checkoutSessionResponse.getLineItems().get(0).getTitle();
            this.imageUrl = checkoutSessionResponse.getLineItems().get(0).getImage().getImageUrl();
            this.quantity = checkoutSessionResponse.getLineItems().get(0).getQuantity();

            if(checkoutSessionResponse.getLineItems().get(0).getShippingDetail() != null){
                this.shippedVia = checkoutSessionResponse.getLineItems().get(0).getShippingDetail().getShippingServiceName();
            }
        }

        this.addressLine = checkoutSessionResponse.getShippingAddress().getAddressLine1();
        this.city = checkoutSessionResponse.getShippingAddress().getCity();
        this.state = checkoutSessionResponse.getShippingAddress().getStateOrProvince();
        this.country = checkoutSessionResponse.getShippingAddress().getCountry();
        this.postalCode = checkoutSessionResponse.getShippingAddress().getPostalCode();
        this.phoneNumber = checkoutSessionResponse.getShippingAddress().getPhoneNumber();

        this.subtotal = checkoutSessionResponse.getPricingSummary().getPriceSubtotal().getValue();
        this.shippingCost = checkoutSessionResponse.getPricingSummary().getDeliveryCost().getValue();
        this.tax = checkoutSessionResponse.getPricingSummary().getTax().getValue();
        this.total = checkoutSessionResponse.getPricingSummary().getTotal().getValue();

        if(checkoutSessionResponse.getProvidedPaymentInstrument()!= null && checkoutSessionResponse.getProvidedPaymentInstrument().getPaymentMethodBrand() != null) {
            this.providedCreditCardBrand = checkoutSessionResponse.getProvidedPaymentInstrument().getPaymentMethodBrand().getPaymentMethodBrandType();
        }
        if(checkoutSessionResponse.getProvidedPaymentInstrument()!= null && checkoutSessionResponse.getProvidedPaymentInstrument().getPaymentInstrumentReference() != null) {
            this.providedCreditCardNumber = checkoutSessionResponse.getProvidedPaymentInstrument().getPaymentInstrumentReference().getLastFourDigitForCreditCard();
        }

    }

}

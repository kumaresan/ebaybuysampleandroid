package com.ebay.api.sample.ebaybuysample.apicall;

import android.util.Log;

/**
 * Get the purchase order from eBay
 * Created by @Kumaresan on 3/1/2017.
 */

public class GetGuestPurchaseOrder extends BaseApiCall {
    private static final String TAG = "GetGuestPurchaseOrder";
    private String orderId;

    public GetGuestPurchaseOrder(String orderId, ApiCallback apiCallback) {
        super(apiCallback);
        this.orderId = orderId;
    }

    protected void callApiInBackground() throws Exception {
        Log.d(TAG, "callApiInBackground");
        this.response = getOrderApi().getGuestPurchaseOrder(this.orderId);
    }
}
